# static-website-example

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Paterne3/static-website-example.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Paterne3/static-website-example/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

# ETAPES : 
- Créez un compte sur gitlab.com
- Créez un nouveau project publique
- Créez votre fichier Dockerfile
- créer une nouvelle branche pour le developpement 
- Créez une variable d'environments (settings-CI/CD--variable-Add variable.)
- Créez votre fichier .gitlab-ci.yml
- Lancez votre pipeline et validez que l'image est bien buildée
# Description du code

1) Fichier Dockerfile 

- Créer une image nginx
- lancer les commandes de mises à jours et l'installations des utilitaires curl et git 
- preparer les repetoires de stockage /usr/share/nginx/html/pour stocker le site web
- git clone le site web et le stocker dans le repertoire  ci-dessous
- lancer le demon nginx

2) Fichier .gitlab-ci.yml
- Declarer les variables de l'environments de deploiement.  
Dans le cas d'experience, j'ai utilisé un API de eazytraining sur python avec son domaine et le port d'ecoute est 1993;
REVIEW_EXTERNAL_PORT:8000
STG_EXTERNAL_PORT: 8080
PROD_EXTERNAL_PORT: 80
TEST_PORT: 90
- declarer notre image Docker
- declarer le service
- lister dans l'ordre chonologique les differents jobs de notre pipeline (Build image, Test acceptation, Release image, Deploy review, Stop review, Deploy staging, Test staging, Deploy prod, Test prod)
  - le job Build image lance l'execution du docker de l'Application web, le sauvergarde en .tar, declarer l'image sauvergarde comme un artefact pour qu'il puisse etre utiliser après en cas de sucess du Build. 
  - le job Test acceptation charge l'image, ensuite lance un conteneur enfin d'effectuer un test curl sur l'alias du service docker de la chaine dimension de l'appliaction web.
  - le job Release charge l'image et applique les tag sur la branche et le commit, il se connecte sur le registre et uploader l'image sur le registre
  - le job Deploy review, c'est un deploiement environements de review. on declare l'environement et le domaine deploiement;il est créé seulement en cas de merge_requests.
  - le job Stop review, on declare l'environement, le domaine deploiement et le script de deploiement dans l'environement;il est créé seulement en cas de merge_requests de facon manuel.
  - le job Deploy staging est la mise en scene,on declare l'environement, le domaine deploiement et le script de deploiement dans l'environement.
  - le job Deploy prod est le job final lorsque tout est validé et il est créé uniquement sur la branche Master.
  
  
